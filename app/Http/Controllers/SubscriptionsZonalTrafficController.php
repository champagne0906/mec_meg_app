<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionsZonalTrafficController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            array(
                'notificationSubscriptionList' => array(
                    'zonalTrafficSubscription' => array(
                        0 => array(
                            'clientCorrelator' => '0123',
                            'resourceURL' => 'http://example.com/exampleAPI/location/v1/subscriptions/zonalTraffic/subscription123',
                            'callbackReference' => array(
                                'notifyURL' => 'http://clientApp.example.com/location_notifications/123456',
                            ),
                            'zoneId' => 'zone01',
                            'interestRealm' => 'LA',
                            'userEventCriteria' => 'Transferring',
                        ),
                        1 => array(
                            'clientCorrelator' => '0124',
                            'resourceURL' => 'http://example.com/exampleAPI/location/v1/subscriptions/zonalTraffic/subscription124',
                            'callbackReference' => array(
                                'notifyURL' => 'http://clientApp.example.com/location_notifications/123457',
                            ),
                            'zoneId' => 'zone02',
                            'interestRealm' => 'LA',
                            'userEventCriteria' => 'Transferring',
                        ),
                    ),
                    'resourceURL' => 'http://example.com/exampleAPI/location/v1/zonalTraffic',
                ),
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(
            array(
                'zonalTrafficSubscription' => array(
                    'clientCorrelator' => '0123',
                    'resourceURL' => 'http://example.com/exampleAPI/location/v1/subscriptions/zonalTraffic/subscription123',
                    'callbackReference' => array(
                        'notifyURL' => 'http://clientApp.example.com/location_notifications/123456',
                    ),
                    'zoneId' => 'zone01',
                    'interestRealm' => 'LA',
                    'userEventCriteria' => 'Transferring',
                ),
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            array(
                'zonalTrafficSubscription' => array(
                    'clientCorrelator' => '0123',
                    'resourceURL' => 'http://example.com/exampleAPI/location/v1/subscriptions/zonalTraffic/subscription123',
                    'callbackReference' => array(
                        'notifyURL' => 'http://clientApp.example.com/location_notifications/123456',
                    ),
                    'zoneId' => 'zone01',
                    'interestRealm' => 'LA',
                    'userEventCriteria' => 'Transferring',
                ),
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response()->json(
            array(
                'zonalTrafficSubscription' => array(
                    'clientCorrelator' => '0123',
                    'resourceURL' => 'http://example.com/exampleAPI/location/v1/subscriptions/zonalTraffic/subscription123',
                    'callbackReference' => array(
                        'notifyURL' => 'http://clientApp.example.com/location_notifications/123456',
                    ),
                    'zoneId' => 'zone01',
                    'interestRealm' => 'LA',
                    'userEventCriteria' => 'Transferring',
                ),
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json();
    }
}
