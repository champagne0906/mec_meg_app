<?php

namespace App\Http\Controllers;

class ZonesController extends Controller
{
    public function index()
    {
        return response()->json([
            "zoneList" => [
                "zone" => [
                    [
                        "zoneId" => "zone01",
                        "numberOfAccessPoints" => "3",
                        "numberOfUnserviceableAccessPoints" => "1",
                        "numberOfUsers" => "10",
                        "resourceURL" => "http://example.com/exampleAPI/location/v1/zones/zone01",
                    ],
                    [
                        "zoneId" => "zone02",
                        "numberOfAccessPoints" => "12",
                        "numberOfUnserviceableAccessPoints" => "0",
                        "numberOfUsers" => "36",
                        "resourceURL" => "http://example.com/exampleAPI/location/v1/zones/zone02",
                    ],
                ],
                "resourceURL" => "http://example.com/exampleAPI/location/v1/zones",
            ],
        ]);
    }

    public function show($zoneId)
    {
        return response()->json([
            'accessPointInfo' => [
                'accessPointID' => $zoneId,
                'locationInfo' => [
                    'latitude' => '80.123',
                    'longitude' => '70.123',
                    'altitude' => '10.0',
                    'accuracy' => '0',
                ],
                'connectionType' => 'Marco',
                'operationStatus' => 'Serviceable',
                'numberOfUsers' => '15',
                'interestRealm' => 'LA',
                'resourceURL' => url()->current(),
            ],
        ]);
    }

    public function accessPointsIndex($zoneId)
    {
        return response()->json([
            'accessPointList' => array(
                'zoneId' => $zoneId,
                'accessPoint' => array(
                    0 => array(
                        'accessPointId' => '001010000000000000000000000000001',
                        'locationInfo' => array(
                            'latitude' => '90.123',
                            'longitude' => '80.123',
                            'altitude' => '10.0',
                            'accuracy' => '0',
                        ),
                        'connectionType' => 'Macro',
                        'operationStatus' => 'Serviceable',
                        'numberOfUsers' => '5',
                        'interestRealm' => 'LA',
                        'resourceURL' => 'http://example.com/exampleAPI/location/v1/zones/zone01/accessPoints/ap001',
                    ),
                    1 => array(
                        'accessPointId' => '001010000000000000000000000000010',
                        'locationInfo' => array(
                            'latitude' => '91.123',
                            'longitude' => '81.123',
                            'altitude' => '12.0',
                            'accuracy' => '1',
                        ),
                        'connectionType' => 'Macro',
                        'operationStatus' => 'Unserviceable',
                        'numberOfUsers' => '0',
                        'interestRealm' => 'DC',
                        'resourceURL' => 'http://example.com/exampleAPI/location/v1/zones/zone01/accessPoints/ap002',
                    ),
                    2 => array(
                        'accessPointId' => '001010000000000000000000000000011',
                        'locationInfo' => array(
                            'latitude' => '93.123',
                            'longitude' => '83.123',
                            'altitude' => '16.0',
                            'accuracy' => '3',
                        ),
                        'connectionType' => 'Macro',
                        'operationStatus' => 'Serviceable',
                        'numberOfUsers' => '5',
                        'interestRealm' => 'NJ',
                        'resourceURL' => 'http://example.com/exampleAPI/location/v1/zones/zone01/accessPoints/ap003',
                    ),
                ),
                'resourceURL' => 'http://example.com/exampleAPI/location/v1/zones/zone01/accessPoints',
            ),
        ]);
    }

    public function accessPointsShow($zoneId, $accessPointId)
    {
        return response()->json([
            'accessPointInfo' => [
                'accessPointID' => $accessPointId,
                'locationInfo' => [
                    'latitude' => '80.123',
                    'longitude' => '70.123',
                    'altitude' => '10.0',
                    'accuracy' => '0',
                ],
                'connectionType' => 'Marco',
                'operationStatus' => 'Serviceable',
                'numberOfUsers' => '15',
                'interestRealm' => 'LA',
                'resourceURL' => url()->current(),
            ],
        ]);
    }
}
