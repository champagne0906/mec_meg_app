<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        return response()->json(
            array (
                'userList' => 
                array (
                  'user' => 
                  array (
                    0 => 
                    array (
                      'address' => 'acr:192.0.2.1',
                      'accessPointId' => '001010000000000000000000000000001',
                      'zoneId' => 'zone01',
                      'resourceURL' => 'http://example.com/exampleAPI/location/v1/users/acr%3A192.0.2.1',
                    ),
                    1 => 
                    array (
                      'address' => 'acr:192.0.2.2',
                      'accessPointId' => '001010000000000000000000000000001',
                      'zoneId' => 'zone01',
                      'resourceURL' => 'http://example.com/exampleAPI/location/v1/users/acr%3A192.0.2.2',
                    ),
                    2 => 
                    array (
                      'address' => 'acr:192.0.2.3',
                      'accessPointId' => '001010000000000000000000000000010',
                      'zoneId' => 'zone01',
                      'resourceURL' => 'http://example.com/exampleAPI/location/v1/users/acr%3A192.0.2.3',
                    ),
                    3 => 
                    array (
                      'address' => 'acr:192.0.2.4',
                      'accessPointId' => '001010000000000000000000000000001',
                      'zoneId' => 'zone02',
                      'resourceURL' => 'http://example.com/exampleAPI/location/v1/users/acr%3A192.0.2.4',
                    ),
                    4 => 
                    array (
                      'address' => 'acr:192.0.2.5',
                      'accessPointId' => '001010000000000000000000000000010',
                      'zoneId' => 'zone02',
                      'resourceURL' => 'http://example.com/exampleAPI/location/v1/users/acr%3A192.0.2.5',
                    ),
                  ),
                  'resourceURL' => 'http://example.com/exampleAPI/location/v1/users',
                ),
              )
        );
    }

    public function show($userId)
    {
        return response()->json([
            'userInfo' => [
                'address' => $userId,
                'accessPointID' => '00101000000000001',
                'zoneId' => 'zone01',
                'resourceURL' => url()->current(),
            ],
        ]);
    }
}
