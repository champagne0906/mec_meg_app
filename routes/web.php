<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('exampleAPI/location/v1/zones', 'ZonesController@index');
Route::get('exampleAPI/location/v1/zones/{zoneId}', 'ZonesController@show');
Route::get('exampleAPI/location/v1/zones/{zoneId}/accessPoints', 'ZonesController@accessPointsIndex');
Route::get('exampleAPI/location/v1/zones/{zoneId}/accessPoints/{accessPointId}', 'ZonesController@accessPointsShow');

Route::get('exampleAPI/location/v1/users', 'UsersController@index');
Route::get('exampleAPI/location/v1/users/{userId}', 'UsersController@show');

Route::resource('exampleAPI/location/v1/subscriptions/zonalTraffic', 'SubscriptionsZonalTrafficController');
Route::resource('exampleAPI/location/v1/subscriptions/userTracking', 'SubscriptionsUserTrackingController');
Route::resource('exampleAPI/location/v1/subscriptions/zonalStatus', 'SubscriptionsZonalStatusController');

Route::get('/', function () {
    return view('welcome');
});

